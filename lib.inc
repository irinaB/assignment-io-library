section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60
	syscall
	ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.loop:
		cmp byte [rdi+rax], 0
		je .exit
		inc rax
		jmp .loop
	.exit:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	xor rax, rax
	push rdi
	call string_length
	pop rdi
	mov rsi, rdi
	mov rdx, rax
	mov rdi, 1
	mov rax, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	xor rax, rax
	push rdi
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	mov rax, 1
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	xor rax, rax
	mov rdi, 0xA
	call print_char
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rax, rax
	mov rax, rdi    
	mov rbx, 10     
	push 0          
	.loop:
		xor rdx, rdx    
		div rbx         
		add rdx, 48     
		push rdx        
		cmp rax, 0      
		jne .loop
	.pr_number:
		pop rdi             
		cmp rdi, 0          
		je .end             
		call print_char     
		jmp .pr_number      
	.end:
		ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	xor rax, rax
	cmp rdi, 0
	jl .signed
	jmp print_uint
	.signed:
		push rdi
		mov rdi, '-'
		call print_char
		pop rdi
		neg rdi
		jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
call string_length               
	xor rax, rax
	xor rcx, rcx
	xor r9, r9
	xor r10, r10
	.loop:
		mov r9b, byte [rdi + rcx]
		mov r10b, byte [rsi + rcx]
		cmp r9b, r10b
		jne .notequals
		cmp r9b, 0
		je .equals
		inc rcx
		jmp .loop
	.equals:
		mov rax, 1
		ret
	.notequals:
		mov rax, 0
		ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax       
	mov rdi, 0          
	mov rdx, 1      
	push 0          
	mov rsi, rsp           
	syscall                
	pop rax                
	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12               
	mov r9, rdi	            
	xor r11,r11	            
	mov r12,rdi
	.skip_tabs:
		push r11
		push rsi
		call read_char
		pop rsi
		pop r11
		cmp rax, 0x0        
		je .end
		cmp rax, 0xA        
		je .skip_tabs
		cmp rax, 0x20       
		je .skip_tabs
		cmp rax, 0x9        
		je .skip_tabs
		mov [r9], al
		inc r9              
		inc r11            
	.read_symbol:
		cmp rsi, r11         
		je .nosymbol
		push r11            
		push rsi
		call read_char
		pop rsi
		pop r11
		cmp rax, 0xA        
		je .end
		cmp rax, 0x20       
		je .end
		cmp rax, 0x9        
		je .end
		cmp rax, 0x0        
		je .end
		mov [r9], rax
		inc r9              
		inc r11             
		jmp .read_symbol
		
    .nosymbol:
		mov rdx, 0
		pop r12
		mov rax, 0
		ret

    .end:
		mov rdx, r11
		mov rax, r12
		mov byte[r9], 0
		pop r12
		ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	mov r11, 0
	push r13
	push r12
	mov r13, 10
	mov r12, 0
	.loop:
		cmp byte[rdi+r11], 0
		je .end
		mov r12b, byte [rdi + r11]
		cmp r12, 0x39
		ja .end
		cmp r12, 0x30
		jb .end
		sub r12, 0x30
		push rdx
		mul r13
		pop rdx
		add rax, r12
		inc r11
		jmp .loop
	.end:
		pop r12
		pop r13
		mov rdx, r11
		ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
	cmp byte[rdi], '-'
	jz .negative
	call parse_uint
	ret
	.negative:
		inc rdi
		call parse_uint
		test rdx, rdx
		jz .end
		inc rdx        
		neg rax
		ret
	.err:
		xor rax, rax
		jmp .end
	.end:
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
cmp rdx, 0
    je copy_error
    xor r10, r10
    mov r10b, byte[rdi]
    mov byte[rsi], r10b
    inc rdi
    inc rsi
    dec rdx
    cmp r10, 0
    jne string_copy
    ret

.copy_error:
    xor rax, rax
    ret
